#include <robotViewer.hpp>
#include <pcl/console/parse.h>
#include <pcl/surface/convex_hull.h>
#include <math.h>       /* isnan, sqrt */
robotViewer::robotViewer():viewer_("Default viewer"), cloudPtr_(new pcl::PointCloud<pcl::PointXYZ>), normal_id_(0){

	spin();
}



void robotViewer::spin(){

	// Define lambda function
	auto f = [this]() {

			viewer_.setBackgroundColor(0.05, 0.05, 0.05, 0); // Setting background to a dark grey
			viewer_.addCoordinateSystem (0.1, "GlobalFrame", 0);
			while (!viewer_.wasStopped ()){ // Display the visualiser until 'q' key is pressed
				mutex_.lock();
				viewer_.spinOnce ();
				mutex_.unlock();
				usleep(10);
			}
    	};

	// Start thread
	th_ = boost::thread(boost::bind<void>(f)); 
}

void robotViewer::thread_interrupt(){

	th_.interrupt();
}

robotViewer::~robotViewer(){

	// Wait the thread to end
	try
	{
		while (!viewer_.wasStopped ());
		
		thread_interrupt();
	}
	catch ( const boost::thread_interrupted& e)
	{
		std::cout << "ERROR in the thread interrupt()" << std::endl;
	}

}

bool robotViewer::getPointCloud(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, const std::string& path){

	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_in (new pcl::PointCloud<pcl::PointXYZ>);

	if (pcl::io::loadPCDFile<pcl::PointXYZ> (path.c_str(), *cloud_in) == -1) //* load the file
	{
		PCL_ERROR ("Couldn't read file %s\n", path);
		return false;
	}

 	std::vector<int> indices;

	pcl::removeNaNFromPointCloud(*cloud_in, *cloud, indices);

	std::cout << "Loaded " << cloud->width * cloud->height << " data points from " << path << std::endl;

	return true;
}

bool robotViewer::loadPointCloud(const std::string& path_ptcloud, const Eigen::Matrix4f transf, const double& r, const double& g, const double& b){

	/* ---------------------------------------
		Load the point cloud
 	---------------------------------------*/
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_in(new pcl::PointCloud<pcl::PointXYZ>);

	if (!getPointCloud(cloud_in, path_ptcloud))
		return false;

	// Apply transformation to the pointlcoud	
	pcl::transformPointCloud (*cloud_in, *cloudPtr_, transf);

	// Load KDtree 
	kdtree_.setInputCloud (cloudPtr_);

	// Set colour
	pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> single_color(cloudPtr_, 255.0 * r, 255.0 * g, 255.0 * b);

	// Load Point cloud into the viewer
	mutex_.lock();
	viewer_.addPointCloud<pcl::PointXYZ> (cloudPtr_, single_color, "sample_cloud");
	viewer_.addCoordinateSystem (0.1, "cloud_frame", 0);
	mutex_.unlock();

	return true;
}


Eigen::Vector3d robotViewer::PointXYZ2Vector3d(const pcl::PointXYZ& pointxyz){
	
	Eigen::Vector3d pointvector3d;
	pointvector3d(0) = pointxyz.x;
	pointvector3d(1) = pointxyz.y;
	pointvector3d(2) = pointxyz.z;

	return pointvector3d;
}

pcl::PointXYZ robotViewer::Vector3d2PointXYZ(const Eigen::Vector3d& pointvector3d){
	
	pcl::PointXYZ pointxyz;
	pointxyz.x = pointvector3d(0);
	pointxyz.y = pointvector3d(1);
	pointxyz.z = pointvector3d(2);

	return pointxyz;
}

void robotViewer::transformPC(const Eigen::Matrix4f& transf){

	mutex_.lock();
	viewer_.removePointCloud("sample_cloud", 0);
	mutex_.unlock();
	
	pcl::transformPointCloud (*cloudPtr_, *cloudPtr_, transf);
	
	mutex_.lock();
	viewer_.addPointCloud<pcl::PointXYZ> (cloudPtr_, "sample_cloud");
	mutex_.unlock();

}

bool robotViewer::plotRobotConf(const std::vector< Eigen::VectorXd > segmentFrames, const std::string& prefix, const double& r, const double& g, const double& b){

	if (segmentFrames.size() == 0){
		std::cout << "[ERROR] segmentFrames is empty." << std::endl;
		return false;
	}

	if (segmentFrames[0].size() < 6){
		std::cout << "[ERROR] size mismatch." << std::endl;
		return false;
	}

	mutex_.lock();
	// Get base frame
	pcl::PointXYZ v0 = Vector3d2PointXYZ(segmentFrames[0].segment(0, 3));
	pcl::PointXYZ v;
	Eigen::VectorXd v_complete;
	// Rotation for the segments' frame
	// Eigen::Affine3f T;
	Eigen::Transform <float , 3, Eigen::Affine, Eigen::DontAlign> T = Eigen::Transform <float , 3, Eigen::Affine, Eigen::DontAlign>::Identity ();

	// Plot base frame
	viewer_.addSphere (v0, 0.007, r, g, b, prefix + "base");
	v_complete = segmentFrames[0];

	T.translate( Eigen::Vector3f(v_complete(0), v_complete(1), v_complete(2)));
	T.rotate(Eigen::AngleAxisf(v_complete(5), Eigen::Vector3f::UnitZ()) * Eigen::AngleAxisf(v_complete(4), Eigen::Vector3f::UnitY()) * Eigen::AngleAxisf(v_complete(3), Eigen::Vector3f::UnitX()));
	
	// Draw base coordinate system
	viewer_.addCoordinateSystem (0.02, T, prefix + "_base_frame");

	// Draw links and joints until the last element
	for (int i = 1; i < segmentFrames.size() - 1; ++i){
		// Save the frame position/orientation
		v_complete = segmentFrames[i];
		
		// Convert the position in a PointXYZ
		v = Vector3d2PointXYZ(v_complete.head(3));

		viewer_.addLine<pcl::PointXYZ> (v0, v, prefix + "_link_" + std::to_string(i));
  		viewer_.addSphere (v, 0.005, r, g, b, prefix + "_joint_" + std::to_string(i));

  		// Frame 
  		T = Eigen::Transform <float , 3, Eigen::Affine >::Identity ();
  		T.translate( Eigen::Vector3f(v_complete(0), v_complete(1), v_complete(2)));
		T.rotate(Eigen::AngleAxisf(v_complete(5), Eigen::Vector3f::UnitZ()) * Eigen::AngleAxisf(v_complete(4), Eigen::Vector3f::UnitY()) * Eigen::AngleAxisf(v_complete(3), Eigen::Vector3f::UnitX()));

		viewer_.addCoordinateSystem (0.02, T, prefix + "_joint_" + std::to_string(i) + "_frame");

		// Store last point
		v0 = v;

	}
	// Plot e-e frame
	v_complete = segmentFrames.back();
	T = Eigen::Transform <float , 3, Eigen::Affine, Eigen::DontAlign>::Identity ();
	T.translate( Eigen::Vector3f(v_complete(0), v_complete(1), v_complete(2)));
	T.rotate(Eigen::AngleAxisf(v_complete(5), Eigen::Vector3f::UnitZ()) * Eigen::AngleAxisf(v_complete(4), Eigen::Vector3f::UnitY()) * Eigen::AngleAxisf(v_complete(3), Eigen::Vector3f::UnitX()));
	
	// Draw last line
	v = Vector3d2PointXYZ(v_complete.head(3));
	viewer_.addLine<pcl::PointXYZ> (v0, v, prefix + "_end");

	// Draw e-e coordinate system
	viewer_.addCoordinateSystem (0.02, T, prefix + "_ee_frame");

	mutex_.unlock();

	return true;
}

void robotViewer::drawCylinder(const double& x, const double& y, const double& z, const double& radius, const Eigen::Vector3d& axis, const double& r, const double& g, const double& b, const std::string& prefix){

	pcl::ModelCoefficients cylinder_coeff;
	cylinder_coeff.values.resize (7);    // We need 7 values
	cylinder_coeff.values[0] = x;
	cylinder_coeff.values[1] = y;
	cylinder_coeff.values[2] = z;
	cylinder_coeff.values[3] = axis(0);
	cylinder_coeff.values[4] = axis(1);
	cylinder_coeff.values[5] = axis(2);
	cylinder_coeff.values[6] = radius;

	mutex_.lock();
	viewer_.addCylinder (cylinder_coeff, prefix);
 	viewer_.setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_COLOR, r, g, b, prefix);
	mutex_.unlock();

}

bool robotViewer::plotPose(const Eigen::VectorXd& pose, const std::string& prefix, const double& r, const double& g, const double& b, const bool& draw_point, const bool& draw_orientation){

	if (pose.size() % 6 != 0){
		std::cout << "[ERROR] size mismatch" << std::endl;
		return false;
	}

	mutex_.lock();
	Eigen::VectorXd v_complete;
	pcl::PointXYZ v, v0;
	Eigen::Transform <float , 3, Eigen::Affine, Eigen::DontAlign> T = Eigen::Transform <float , 3, Eigen::Affine, Eigen::DontAlign>::Identity ();

	// Draw the first element of the trajectory
	v_complete = pose.segment(0, 6);
	v0 = Vector3d2PointXYZ(v_complete.head(3));
	
	if (draw_point)
		viewer_.addSphere (v0, 0.004, r, g, b, prefix + "_start");

	double scale = 0.03;

	if (draw_orientation){
		T.translate( Eigen::Vector3f(v_complete(0), v_complete(1), v_complete(2)));
		T.rotate(Eigen::AngleAxisf(v_complete(5), Eigen::Vector3f::UnitZ()) * Eigen::AngleAxisf(v_complete(4), Eigen::Vector3f::UnitY()) * Eigen::AngleAxisf(v_complete(3), Eigen::Vector3f::UnitX()));		
		viewer_.addCoordinateSystem (scale, T, prefix + "_start_frame");
	}

	// Draw middle points of the trajectory
	for (int i = 1; i < pose.size() / 6 - 1; ++i){
		// Save the frame position/orientation
		v_complete = pose.segment(6 * i, 6);
		
		// Convert the position in a PointXYZ
		v = Vector3d2PointXYZ(v_complete.head(3));
		
		if (draw_point)
			viewer_.addSphere (v, 0.002, r, g, b, prefix + "_point_" + std::to_string(i));
		viewer_.addLine<pcl::PointXYZ> (v0, v, prefix + "_line_" + std::to_string(i));

		if (draw_orientation){
	  		// Frame 
	  		T = Eigen::Transform <float , 3, Eigen::Affine, Eigen::DontAlign>::Identity ();
	  		T.translate( Eigen::Vector3f(v_complete(0), v_complete(1), v_complete(2)));
			T.rotate(Eigen::AngleAxisf(v_complete(5), Eigen::Vector3f::UnitZ()) * Eigen::AngleAxisf(v_complete(4), Eigen::Vector3f::UnitY()) * Eigen::AngleAxisf(v_complete(3), Eigen::Vector3f::UnitX()));

			viewer_.addCoordinateSystem (scale, T, prefix + "_point_" + std::to_string(i) + "_frame");
		}

		// Store last point
		v0 = v;
	}

	// Plot last point
	v_complete = pose.tail(6);
	
	if (draw_orientation){
	 	// Frame 	
		T = Eigen::Transform <float , 3, Eigen::Affine, Eigen::DontAlign>::Identity ();
		T.translate( Eigen::Vector3f(v_complete(0), v_complete(1), v_complete(2)));
		T.rotate(Eigen::AngleAxisf(v_complete(5), Eigen::Vector3f::UnitZ()) * Eigen::AngleAxisf(v_complete(4), Eigen::Vector3f::UnitY()) * Eigen::AngleAxisf(v_complete(3), Eigen::Vector3f::UnitX()));
		viewer_.addCoordinateSystem (0.01, T, prefix + "_point_frame");
	}

	// Draw last line
	v = Vector3d2PointXYZ(v_complete.head(3));
	viewer_.addLine<pcl::PointXYZ> (v0, v, prefix + "_end");
	if (draw_point)
		viewer_.addSphere (v, 0.004, r, g, b, prefix + "_point_end");
	mutex_.unlock();
	return true;
}

bool robotViewer::plotTrajectory(const std::vector<Eigen::VectorXd >& trj_v, const std::string& prefix, const double& r, const double& g, const double& b, const bool& draw_point, const bool& draw_orientation){

	if (trj_v.size() == 0){
		std::cout << "[ERROR] size mismatch." << std::endl;
		return false;
	}

	if (trj_v[0].size() < 6){
		std::cout << "[ERROR] size mismatch." << std::endl;
		return false;
	}

	// Create trajectory vector
	Eigen::VectorXd pose;
	pose.resize(trj_v.size() * 6);

	for (int i = 0; i < trj_v.size(); ++i)
		pose.segment(6 * i, 6) = trj_v[i];
	
	
	return plotPose(pose, prefix, r, g, b, draw_point, draw_orientation);
}


bool robotViewer::computeTpM(const pcl::PointCloud<pcl::PointXYZ>::Ptr cloudPtr, const pcl::PointXYZ& point, Eigen::Vector4f& plane_params, float& curvature, Eigen::Vector3d& pl_point){

	pcl::KdTreeFLANN<pcl::PointXYZ> kdtree;

  	kdtree.setInputCloud (cloudPtr);

	std::vector<int> pointIdxNKNSearch(100);
  	std::vector<float> pointNKNSquaredDistance(100);
  	if ( kdtree.nearestKSearch (point, 100, pointIdxNKNSearch, pointNKNSquaredDistance) <= 0 ){
  		PCL_ERROR ("Kdtree ERROR\n");
  		return false;
  	}

	pcl::computePointNormal(*cloudPtr, pointIdxNKNSearch, plane_params, curvature);

	pl_point = PointXYZ2Vector3d(cloudPtr->points[pointIdxNKNSearch[0]]);

	return true;
}

bool robotViewer::computeTpM(const pcl::PointXYZ& point, Eigen::Vector4f& plane_params, float& curvature, Eigen::Vector3d& pl_point){

    std::vector<int> pointIdxNKNSearch(100);
    std::vector<float> pointNKNSquaredDistance(100);
    
    if ( kdtree_.nearestKSearch (point, 100, pointIdxNKNSearch, pointNKNSquaredDistance) <= 0 ){
        PCL_ERROR ("Kdtree ERROR\n");
        return false;
    }

    pcl::computePointNormal(*cloudPtr_, pointIdxNKNSearch, plane_params, curvature);

    pl_point = PointXYZ2Vector3d(cloudPtr_->points[pointIdxNKNSearch[0]] );

    return true;
}

void robotViewer::drawPoint(const Eigen::Vector3d& p, const Eigen::Vector3d& rgb, const double& size, const std::string& name){
	viewer_.addSphere (Vector3d2PointXYZ(p), size, rgb(0), rgb(1), rgb(2), name.c_str());
}

void robotViewer::drawPoint(const pcl::PointXYZ& p, const Eigen::Vector3d& rgb, const double& size, const std::string& name){
	viewer_.addSphere (p, size, rgb(0), rgb(1), rgb(2), name.c_str());
}

void robotViewer::drawSegments(const std::vector<Eigen::Vector3d>& p0, const std::vector<Eigen::Vector3d>& p1, const Eigen::Vector3d& rgb, const std::string& prefix){

	// Check size
	assert(p0.size() == p1.size());

	pcl::PointXYZRGB pcl_p0, pcl_p1;

	for (int i = 0; i < p0.size(); ++i){

		std::string s = prefix + 's' + std::to_string(i);
		std::string l = prefix + 'l' + std::to_string(i);

		// First Point
		pcl_p0.x = p0[i](0);
		pcl_p0.y = p0[i](1);
		pcl_p0.z = p0[i](2);
		// Second Point
		pcl_p1.x = p1[i](0);
		pcl_p1.y = p1[i](1);
		pcl_p1.z = p1[i](2);

		mutex_.lock();
		viewer_.addLine<pcl::PointXYZRGB> (pcl_p0, pcl_p1, rgb(0), rgb(1), rgb(2), l.c_str());
		viewer_.setShapeRenderingProperties (pcl::visualization::PCL_VISUALIZER_LINE_WIDTH, 3, l.c_str());
		mutex_.unlock();
	}
}

void robotViewer::drawLine(const std::vector<Eigen::Vector3d>& line, const double& r, const double& g, const double& b, const std::string& prefix, const double& line_width){

	pcl::PointXYZRGB pcl_p0, pcl_p1;

	for (int i = 1; i < line.size(); ++i){

		std::string s = prefix + 's' + std::to_string(i);
		std::string l = prefix + 'l' + std::to_string(i);

		// First Point
		pcl_p0.x = line[i - 1](0);
		pcl_p0.y = line[i - 1](1);
		pcl_p0.z = line[i - 1](2);
		// Second Point
		pcl_p1.x = line[i](0);
		pcl_p1.y = line[i](1);
		pcl_p1.z = line[i](2);

		mutex_.lock();
		viewer_.addLine<pcl::PointXYZRGB> (pcl_p0, pcl_p1, r, g, b, l.c_str());
		viewer_.setShapeRenderingProperties (pcl::visualization::PCL_VISUALIZER_LINE_WIDTH, line_width, l.c_str());
		mutex_.unlock();
	}
}


void robotViewer::setBackgroundColor(const double& r, const double& g, const double& b){
	mutex_.lock();
	viewer_.setBackgroundColor (r, g, b);
	mutex_.unlock();
}

void robotViewer::setCameraPosition(const double& x, const double& y, const double& z, const double& theta, const double& phi, const double& psi){
	mutex_.lock();
	viewer_.setCameraPosition (x, y, z, theta, phi, psi);
	mutex_.unlock();
}

void robotViewer::showNormals(const int& number, const std::string& prefix){

	// Random vars
 	std::random_device rd;
 	std::mt19937 gen(rd());
    std::uniform_int_distribution<> disInt(0.0, PointCloudSize() - 1);

	Eigen::Vector3d point;
    for (int i = number; --i;){

		// Get a random point from the point cloud
		point = robotViewer::PointXYZ2Vector3d(getPointCloudPoint((int) disInt(gen)));

		// Draw Normal
		drawNormal(point, prefix + std::to_string(i));
	}
}

bool robotViewer::drawNormal(const Eigen::Vector3d& point, const std::string& prefix, const Eigen::Vector3d& rgb, const double& scale){

	Eigen::Vector4f plane_params;
	float curvature;
	Eigen::Vector3d pl_point;

	// Get the tangent plane
	if (!computeTpM(Vector3d2PointXYZ(point), plane_params, curvature, pl_point))
		return false;

	// Normal Vector

	Eigen::Vector3d normal_vec;

	normal_vec(0) = plane_params(0);
	normal_vec(1) = plane_params(1);
	normal_vec(2) = plane_params(2);

	drawVector(point, normal_vec, prefix, rgb, scale);	

    return true;
}

bool robotViewer::drawVector(const Eigen::Vector3d& origin_pont, const Eigen::Vector3d& vec, const std::string& prefix, const Eigen::Vector3d& rgb, const double& scale){

	// Get tip point
	Eigen::Vector3d tip_point = origin_pont + scale * vec;

	// Create vector for the segment line 
	std::vector<Eigen::Vector3d> p0, p1;

	p0.push_back(origin_pont);
	p1.push_back(tip_point);
		
	// Draw a single vector
	drawSegments(p0, p1, rgb, prefix + "_seg");

	// Add cone to the arrow
	double arrow_lenght = 0.008;
	double arrow_base_ray = 15;

	pcl::ModelCoefficients coefficients;
	coefficients.values.push_back (tip_point(0) + arrow_lenght * vec(0));
	coefficients.values.push_back (tip_point(1) + arrow_lenght * vec(1));
	coefficients.values.push_back (tip_point(2) + arrow_lenght * vec(2));
	coefficients.values.push_back (-arrow_lenght * vec(0));
	coefficients.values.push_back (-arrow_lenght * vec(1));
	coefficients.values.push_back (-arrow_lenght * vec(2));
	coefficients.values.push_back (arrow_base_ray);
	
	mutex_.lock();
 	viewer_.addCone(coefficients, prefix + "_tip");
 	viewer_.setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_COLOR, rgb(0), rgb(1), rgb(2), prefix + "_tip");
    mutex_.unlock();

}

	
void robotViewer::drawEllipsoid(const Eigen::Vector3d& axis_size, const Eigen::Matrix4f transf, const std::string& prefix, const Eigen::Vector3d& rgb){

    pcl::PointCloud<pcl::PointXYZ>::Ptr ellipsoid_cloud ( new pcl::PointCloud<pcl::PointXYZ> );

	pcl::PointXYZ point;

	// Fill in the point cloud

    for (float theta = 0; theta <= M_PI; theta += 0.1){
    	for (float phi = 0.0; phi < 2 * M_PI; phi += 0.1){

            point.x = axis_size(0) * sinf(theta) * cos(phi);
            point.y = axis_size(1) * sinf(theta) * sinf(phi);
            point.z = axis_size(2) * cosf(theta);

           	ellipsoid_cloud->points.push_back ( point );
    	}
    }

    ellipsoid_cloud->width = ( int ) ellipsoid_cloud->points.size ();
    ellipsoid_cloud->height = 1;

	pcl::transformPointCloud (*ellipsoid_cloud, *ellipsoid_cloud, transf);

    // Show the pointcloud
	mutex_.lock();
	viewer_.addPolygon<pcl::PointXYZ>(ellipsoid_cloud, rgb(0), rgb(1), rgb(2), prefix + "_elps");
	mutex_.unlock();
}

