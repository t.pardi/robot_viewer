// Author: Tommaso Pardi

#ifndef ROBOTVIEWER_CLASS_H
#define ROBOTVIEWER_CLASS_H

// pcl
#include <pcl/point_types.h>
#include <pcl/common/common_headers.h>
#include <pcl/features/normal_3d.h>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/io/ascii_io.h>
#include <pcl/io/vtk_lib_io.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/console/parse.h>
#include <pcl/filters/filter.h>
#include <pcl/filters/passthrough.h>
#include <pcl/common/transforms.h>


#include <boost/thread.hpp>
#include <mutex>

// General purpose lib
#include <iostream>
#include <random>
// Eigen
#include <Eigen/SVD>
#include <Eigen/Geometry>

#define VERBOSE true

class robotViewer{

public:

	/**
	 * robotViewer contructor
	 */
	robotViewer();
	/**
	 * robotViewer destructor
	 */
	~robotViewer();
	/**
	 * Load a point cloud given the path and the calibration matrix of the camera
	 */
	bool loadPointCloud(const std::string&, const Eigen::Matrix4f transf = Eigen::Matrix4f::Identity(), const double& r = 1, const double& g = 1, const double& b = 1);
	/**
	 * Function to convert PointXYZ to Vector3d
	 */
	static Eigen::Vector3d PointXYZ2Vector3d(const pcl::PointXYZ&);
	/**
	 * Function to convert Vector3d to PointXYZ
	 */
	static pcl::PointXYZ Vector3d2PointXYZ(const Eigen::Vector3d&);	
	/**
	 * It applys a transformation to the loaded pointcloud
	 */
	void transformPC(const Eigen::Matrix4f&);
	/**
	 * Plot robot configuration given the pose of each link
	 */
	bool plotRobotConf(const std::vector<Eigen::VectorXd >, const std::string&, const double&, const double&, const double&);
	/**
	 * Plot a point's pose
	 */
	bool plotPose(const Eigen::VectorXd&, const std::string&, const double&, const double&, const double&, const bool& draw_point = true, const bool& draw_orientation = false);
	/**
	 * Plot a trajectory of points' pose
	 */
	bool plotTrajectory(const std::vector<Eigen::VectorXd >&, const std::string&, const double&, const double&, const double&, const bool& draw_point = true, const bool& draw_orientation = false);
	/**
	 * Get the tangent plane to the pointcloud, given a point as input -- static
	 */
	static bool computeTpM(const pcl::PointCloud<pcl::PointXYZ>::Ptr, const pcl::PointXYZ&, Eigen::Vector4f&, float&, Eigen::Vector3d&);
	/**
	 * Get the tangent plane to the pointcloud, given a point as input -- member function
	 */
	bool computeTpM(const pcl::PointXYZ&, Eigen::Vector4f&, float&, Eigen::Vector3d&);
	/**
	 * Show normals over the point cloud
	 */
	void showNormals(const int& number = 100, const std::string& prefix = "normals_");
	/**
	 * Draw normal of a point
	 */
	bool drawNormal(const Eigen::Vector3d&, const std::string&, const Eigen::Vector3d& rgb = {0.0, 0.7, 1.0}, const double& scale = 0.05);
	/**
	 * Draw a point given  Vector3d 
	 */
	void drawPoint(const Eigen::Vector3d&, const Eigen::Vector3d&, const double&, const std::string&);
	/**
	 * Draw a point given a PointXYZ
	 */
	void drawPoint(const pcl::PointXYZ&, const Eigen::Vector3d&, const double&, const std::string&);
	/**
	 * Draw an ellipsoid -- TO CHECK
	 */
	void drawEllipsoid(const Eigen::Vector3d&, const Eigen::Matrix4f transf = Eigen::Matrix4f::Identity(), const std::string& prefix = "_", const Eigen::Vector3d& rgb = {0.9, 0.5, 0.1});

	/**
	 * Get the size of the point cloud
	 */
	int inline PointCloudSize() const {
		return cloudPtr_->points.size();
	}
	/**
	 * Get the n-th element of the point cloud
	 */
	pcl::PointXYZ inline getPointCloudPoint(const int idx) const {
		return cloudPtr_->points[idx];
	}
	/**
	 * Draw a set of straight lines
	 */
	void drawSegments(const std::vector<Eigen::Vector3d>&, const std::vector<Eigen::Vector3d>&, const Eigen::Vector3d& rgb = {0, 0, 1}, const std::string& prefix = "seg");
	/**
	 * Draw a single line
	 */
	void drawLine(const std::vector<Eigen::Vector3d>&, const double&, const double&, const double&, const std::string& prefix = "_", const double& line_width = 3);
	/**
	 * Draw a vector
	 */
	bool drawVector(const Eigen::Vector3d&, const Eigen::Vector3d&, const std::string& prefix = "vec", const Eigen::Vector3d& rgb = {0, 0, 1}, const double& scale = 0.05);
	/**
	 * Set the viewer background
	 */
	void setBackgroundColor(const double&, const double&, const double&);
	/**
	 * Set the viewer camera
	 */
	void setCameraPosition(const double&, const double&, const double&, const double&, const double&, const double&);
	/**
	 * Draw a cylinder
	 */
	void drawCylinder(const double&, const double&, const double&, const double&, const Eigen::Vector3d&, const double&, const double&, const double&, const std::string& str = " ");

private:

	/**
	 * Get the pointcloud and return it by pointer
	 */
	bool getPointCloud(pcl::PointCloud<pcl::PointXYZ>::Ptr, const std::string&);

	/**
	 * Start the inner thread
	 */
	void spin();
	/**
	 * Interrupt the thread before the destruction of the class
	 */
	void thread_interrupt();

	/**
	 * Class vars
	 */

	pcl::visualization::PCLVisualizer viewer_;
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloudPtr_;
	pcl::KdTreeFLANN<pcl::PointXYZ> kdtree_;

	int normal_id_;

	boost::thread th_;
	std::mutex mutex_;
};


#endif