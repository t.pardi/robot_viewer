[![License](https://img.shields.io/badge/License-Apache_2.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

The Robot Viewer class is a wrapper for PCL and KDL, which provides an easy and ready to use collection of functions to 
print the robot and point cloud features, like normals. It also provides the user with function to compute the tangent 
plane to a point on a point cloud.

# Requirements:

- cmake >= 3.4
- Eigen>=3.0
- orocos-kdl
- boost
- thread
- PCL>=1.8

# Install:

    cd <YOUR_PATH>/robot_viewer
    mkdir build
    cd build
    
    cmake .. 

If you want to build the examples use the next code instead:

    cmake -DBUILD_EXAMPLE=ON .. 

Then:

    make -j 4
    sudo make install
