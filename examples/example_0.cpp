#include <cstdlib>
#include <robotViewer.hpp>
#include <iostream>

int main(){

	robotViewer rV;

	/* ---------------------------------------
		Load the point cloud
 	---------------------------------------*/
	
	// Camera Calibration
	Eigen::Matrix4f Camera_Calibration = Eigen::Matrix4f::Identity();

	Camera_Calibration <<	-0.0022 ,   0.8778  , -0.4791, 1.0428,
						    0.9986 ,  -0.0237  , -0.0480, 0.0194,
						   -0.0535 ,  -0.4785  , -0.8765, 1.0148,
						   0, 0, 0, 1;

	rV.loadPointCloud("input_pointcloud.pcd", Camera_Calibration);
	
	// --------------------------------------------
	// -----Open 3D viewer and add point cloud-----
	// --------------------------------------------	
	
	rV.setBackgroundColor (.6,.6,.6);
	rV.setCameraPosition(-1.05428, -0.965422, 1.03604, 0.461031, 0.242206, 0.85369);

	Eigen::Vector3d point = robotViewer::PointXYZ2Vector3d(rV.getPointCloudPoint(1000));

	// Test single normal
	rV.drawNormal(point, "normal");

	// Test multi normal
	rV.showNormals(10);

	// Test draw ellipsoid 
	Eigen::Matrix4f transf = Eigen::Matrix4f::Identity();

	transf(0, 3) = 0.3;
	transf(2, 3) = 0.3; 

	rV.drawEllipsoid(Eigen::Vector3d{0.1, 0.05, 0.15}, transf);

	// Test robot
	robotViewer::PointXYZ2Vector3d(pcl::PointXYZ(0,0,0));

	Eigen::VectorXd a(6);

	std::vector<Eigen::VectorXd > v;

	v.push_back(a);
	a(0) += + 0.1;
	a(4) += M_PI /2;
	v.push_back(a);
	a(1) += + 0.1;
	a(4) += -M_PI /2;
	a(3) += -M_PI /2;
	v.push_back(a);
	a(1) += + 0.1;
	v.push_back(a);

    const std::string prefix = "trj";
    const double r = 1.0;
    const double g = 0.0;
    const double b = 0.0;
    const bool draw_point_flag = true;

	rV.plotTrajectory(v, prefix, r, g, b, draw_point_flag);

	return 0;
}